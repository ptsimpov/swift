let firstName = "Pavel"
let secondName = "Timpov"
let myAge = 34
let myWeight = 82.5
let myHeight = 1.80
let myHobby = "learning the Swift programming language"

print ("My name is - \(firstName) \(secondName). \nMy height is \(myHeight) kg., and my weight is \(myWeight). \nI am \(myAge) years old.\nI really like listen music, watching movies, walking in the fresh air, and \(myHobby)!")

let myWorkOrganization = "Pizzarotti Construction SM S.R.L."
let myWorkFunction = "Accounting"
let startWork = "16 april 2016"
let startWorkDay = "08:00"
let endWorkDay = "17:00"

print("I work in company \(myWorkOrganization) beginning with \(startWork). My work function is \(myWorkFunction). My working day begin in \(startWorkDay) and ends in \(endWorkDay).")
